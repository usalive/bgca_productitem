﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Windows.Forms;

namespace BGCA
{
    public partial class Form1 : Form
    {
        private string AccessToken = "Token";
        public Form1()
        {
            InitializeComponent();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx";
            openFileDialog1.InitialDirectory = "C://Desktop";
            openFileDialog1.Title = "Select excel file to be upload.";
            openFileDialog1.FilterIndex = 1;
            GenerateToken();
        }

        private void GenerateToken()
        {
            try
            {
                string asiBaseAddress = System.Configuration.ConfigurationManager.AppSettings["ASIBaseAddress"].ToString();
                string asiUserName = System.Configuration.ConfigurationManager.AppSettings["ASIusername"].ToString();
                string asiPassword = System.Configuration.ConfigurationManager.AppSettings["ASIpassword"].ToString();
                string asiGrantType = System.Configuration.ConfigurationManager.AppSettings["ASIgrant_type"].ToString();

                var client = new HttpClient();
                client.BaseAddress = new Uri(asiBaseAddress);
                var request = new HttpRequestMessage(HttpMethod.Post, "token");

                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("username", asiUserName));
                keyValues.Add(new KeyValuePair<string, string>("password", asiPassword));
                keyValues.Add(new KeyValuePair<string, string>("grant_type", asiGrantType));

                request.Content = new FormUrlEncodedContent(keyValues);
                var responseMessage = client.SendAsync(request);
                var responseJSONData = responseMessage.Result.Content.ReadAsStringAsync().Result;

                var Replace_responseJSONData = responseJSONData.Replace(".issued", "issued").Replace(".expires", "expires");

                TokenResponse tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(Replace_responseJSONData);

                // access token is not null and error not occured occured
                if (string.IsNullOrWhiteSpace(tokenResponse.error) && !string.IsNullOrWhiteSpace(tokenResponse.access_token))
                    AccessToken = tokenResponse.access_token;
                else
                    txtResponse.Text = "Error occured : " + tokenResponse.error + "\r\n";
            }
            catch (Exception ex)
            {
                txtResponse.Text = "Error occured : " + "\r\n";

                if (!string.IsNullOrEmpty(ex.Message))
                    txtResponse.Text += ex.Message + "\r\n";
                if (ex.InnerException != null)
                {
                    if (!string.IsNullOrEmpty(ex.InnerException.Message))
                        txtResponse.Text += "\nInner exception : " + ex.InnerException.Message + "\r\n";
                }
            }
        }

        private void btnBrowseFile_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(AccessToken))
                    MessageBox.Show("Access token not generated");
                else
                {
                    if (openFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        if (openFileDialog1.CheckFileExists)
                        {
                            string FullPath = Path.GetFullPath(openFileDialog1.FileName);
                            string Extension = Path.GetExtension(openFileDialog1.FileName);

                            if (Extension.ToLower() == ".xlsx")
                                txtFileName.Text = FullPath;
                            else
                            {
                                openFileDialog1.FileName = null;
                                txtFileName.Text = null;
                                MessageBox.Show("Please upload .xlsx (excel) file only");
                            }
                        }
                    }
                    else
                    {
                        openFileDialog1.FileName = null;
                        txtFileName.Text = null;
                        MessageBox.Show("Please Upload document.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSubmitFile_Click(object sender, EventArgs e)
        {
            //lblStatus.Text = "";
            //lblMessage.Text = "";
            //lblErrorCode.Text = "";

            txtResponse.Text = "";
            txtExcelRowError.Text = "";

            string FullFilePath = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(AccessToken))
                    MessageBox.Show("Access token not generated");
                else
                {
                    string Filename = Path.GetFileName(openFileDialog1.FileName);

                    if (string.IsNullOrWhiteSpace(Filename))
                        MessageBox.Show("Please select a valid document.");
                    else
                    {
                        #region Financial Entity Api call
                        var financialentity_client = new RestClient("https://bgcascripting.imiscloud.com/api/financialentity");
                        var financialentity_request = new RestRequest(Method.GET);

                        financialentity_request.AddHeader("cache-control", "no-cache");
                        financialentity_request.AddHeader("Content-Type", "application/json");
                        financialentity_request.AddHeader("Authorization", "Bearer " + AccessToken);

                        IRestResponse financialentity_response = financialentity_client.Execute(financialentity_request);
                        #endregion

                        #region GLAccount Entity Api call
                        var glaccount_client = new RestClient("https://bgcascripting.imiscloud.com/api/glaccount");
                        var glaccount_request = new RestRequest(Method.GET);

                        glaccount_request.AddHeader("cache-control", "no-cache");
                        glaccount_request.AddHeader("Content-Type", "application/json");
                        glaccount_request.AddHeader("Authorization", "Bearer " + AccessToken);

                        IRestResponse glaccount_response = glaccount_client.Execute(glaccount_request);
                        #endregion

                        #region Tax Category Entity Api call
                        var taxcategory_client = new RestClient("https://bgcascripting.imiscloud.com/api/taxcategory");
                        var taxcategory_request = new RestRequest(Method.GET);

                        taxcategory_request.AddHeader("cache-control", "no-cache");
                        taxcategory_request.AddHeader("Content-Type", "application/json");
                        taxcategory_request.AddHeader("Authorization", "Bearer " + AccessToken);

                        IRestResponse taxcategory_response = taxcategory_client.Execute(taxcategory_request);
                        #endregion

                        if (financialentity_response.StatusCode == System.Net.HttpStatusCode.OK &&
                            glaccount_response.StatusCode == System.Net.HttpStatusCode.OK &&
                            taxcategory_response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            #region Get FinancialEntity Items -> $values in Jarray
                            JValue financialentity_value = (JValue)financialentity_response.Content;
                            var financialentity_obj = JObject.Parse(financialentity_value.Value.ToString());
                            JArray financialentity_array = (JArray)financialentity_obj["Items"]["$values"];
                            #endregion

                            #region Get GLAccount Items -> $values in Jarray
                            JValue glaccount_value = (JValue)glaccount_response.Content;
                            var glaccount_obj = JObject.Parse(glaccount_value.Value.ToString());
                            JArray glaccount_array = (JArray)glaccount_obj["Items"]["$values"];
                            #endregion

                            #region Get Tax Category Items -> $values in Jarray
                            JValue taxcategory_value = (JValue)taxcategory_response.Content;
                            var taxcategory_obj = JObject.Parse(taxcategory_value.Value.ToString());
                            JArray taxcategory_array = (JArray)taxcategory_obj["Items"]["$values"];


                            JArray taxcategoryfinalArray = new JArray();
                            foreach (var items in taxcategory_array)
                            {
                                taxcategoryfinalArray.Add(items["Properties"]["$values"]);
                            }
                            #endregion

                            #region Save Excel file
                            string FolderPath = Application.StartupPath + "\\Document\\";

                            if (!Directory.Exists(FolderPath))
                                Directory.CreateDirectory(FolderPath);

                            Filename = Guid.NewGuid().ToString() + "_" + DateTime.Now.Date.ToString("yyyy_MM_dd") + ".xlsx";

                            FullFilePath = FolderPath + Filename;

                            File.Copy(openFileDialog1.FileName, FullFilePath);
                            #endregion

                            // once file save read all rows
                            FileInfo ReadExcelFile = new FileInfo(FullFilePath);

                            using (ExcelPackage package = new ExcelPackage(ReadExcelFile))
                            {
                                ExcelWorksheet workSheet = package.Workbook.Worksheets["Sheet1"];
                                int TotalRows = workSheet.Dimension.Rows;

                                if (TotalRows <= 1)
                                    MessageBox.Show("No data found in excel to process");
                                else
                                {
                                    JObject jobj = null;

                                    // read JSON directly from a file                                
                                    using (StreamReader file = File.OpenText(Application.StartupPath + "\\samplejson1.json"))
                                    {
                                        using (JsonTextReader reader = new JsonTextReader(file))
                                        {
                                            jobj = (JObject)JToken.ReadFrom(reader);
                                        }
                                    }
                                    if (jobj != null)
                                    {
                                        //JArray jArray = new JArray();

                                        for (int i = 2; i <= TotalRows; i++)
                                        {
                                            try
                                            {
                                                JObject tmpjObject = new JObject(jobj);
                                                Guid ItemIdobj = Guid.NewGuid();

                                                #region Read Excel Data Cell

                                                string Name = Convert.ToString(workSheet.Cells[i, 1].Value);
                                                string ProductCode = Convert.ToString(workSheet.Cells[i, 2].Value);

                                                string EffectiveDate = Convert.ToString(workSheet.Cells[i, 3].Value);
                                                string EndingDate = Convert.ToString(workSheet.Cells[i, 4].Value);

                                                string PriceStandard = Convert.ToString(workSheet.Cells[i, 5].Value);
                                                string FinancialEntity = Convert.ToString(workSheet.Cells[i, 6].Value);

                                                string AccountsReceivable = Convert.ToString(workSheet.Cells[i, 7].Value);
                                                string Income = Convert.ToString(workSheet.Cells[i, 8].Value);

                                                string Description = Convert.ToString(workSheet.Cells[i, 9].Value);
                                                string TaxCategory = Convert.ToString(workSheet.Cells[i, 10].Value);
                                                string AccountingMethod = Convert.ToString(workSheet.Cells[i, 11].Value);

                                                #endregion

                                                #region Set values in Jobject

                                                string TaxCategoryGuid = string.Empty;
                                                bool isTaxcategoryMatch = false;

                                                // Set tax category
                                                if (string.IsNullOrWhiteSpace(TaxCategory) == false)
                                                {
                                                    foreach (var items in taxcategoryfinalArray)
                                                    {
                                                        foreach (var item in items)
                                                        {
                                                            if (item["Name"]?.ToString() == "TaxCategoryKey")
                                                            {
                                                                TaxCategoryGuid = Convert.ToString(item["Value"]["$value"]);
                                                            }
                                                            if (item["Value"]?.ToString().ToLower() == TaxCategory.ToLower())
                                                            {
                                                                isTaxcategoryMatch = true;
                                                                break;
                                                            }
                                                        }
                                                        if (isTaxcategoryMatch)
                                                        {
                                                            break;
                                                        }
                                                    }
                                                }

                                                if (isTaxcategoryMatch)
                                                {
                                                    // Set taxcategory guid
                                                    tmpjObject["ItemFinancialInformation"]["TaxCategory"]["TaxCategoryId"] = TaxCategoryGuid;
                                                }
                                                else
                                                {
                                                    // remove ItemFinancialAccounts -> AccountsReceivable
                                                    var t = (JObject)tmpjObject.SelectToken("ItemFinancialInformation");
                                                    t.Property("TaxCategory").Remove();
                                                }

                                                // Set accounting method
                                                if (string.IsNullOrWhiteSpace(AccountingMethod) == false)
                                                {
                                                    if (AccountingMethod.ToLower() == "cash")
                                                    {
                                                        // remove ItemFinancialInformation -> AccountingMethod
                                                        var t = (JObject)tmpjObject.SelectToken("ItemFinancialInformation");
                                                        t.Property("AccountingMethod").Remove();
                                                    }
                                                }

                                                if (string.IsNullOrWhiteSpace(AccountsReceivable) == false)
                                                {
                                                    var temp_glaccount_array = glaccount_array.Where(x => x["GLAccountCode"]?.ToString() == AccountsReceivable).FirstOrDefault();

                                                    if (temp_glaccount_array != null)
                                                    {
                                                        string GLAccountName = Convert.ToString(temp_glaccount_array["Name"]);
                                                        string GLAccountId = Convert.ToString(temp_glaccount_array["GLAccountId"]);

                                                        tmpjObject["ItemFinancialInformation"]["ItemFinancialAccounts"]["AccountsReceivable"]["GLAccount"]["GLAccountId"] = GLAccountId;
                                                        tmpjObject["ItemFinancialInformation"]["ItemFinancialAccounts"]["AccountsReceivable"]["GLAccount"]["GLAccountCode"] = AccountsReceivable;
                                                        tmpjObject["ItemFinancialInformation"]["ItemFinancialAccounts"]["AccountsReceivable"]["GLAccount"]["Name"] = GLAccountName;
                                                    }
                                                    else
                                                    {
                                                        // remove ItemFinancialAccounts -> AccountsReceivable
                                                        var t = (JObject)tmpjObject.SelectToken("ItemFinancialInformation.ItemFinancialAccounts");
                                                        t.Property("AccountsReceivable").Remove();
                                                    }
                                                }
                                                else
                                                {
                                                    // remove ItemFinancialAccounts -> AccountsReceivable
                                                    var t = (JObject)tmpjObject.SelectToken("ItemFinancialInformation.ItemFinancialAccounts");
                                                    t.Property("AccountsReceivable").Remove();
                                                }

                                                if (string.IsNullOrWhiteSpace(Income) == false)
                                                {
                                                    var temp_glaccount_array = glaccount_array.Where(x => x["GLAccountCode"]?.ToString() == Income).FirstOrDefault();

                                                    if (temp_glaccount_array != null)
                                                    {
                                                        string GLAccountName = Convert.ToString(temp_glaccount_array["Name"]);
                                                        string GLAccountId = Convert.ToString(temp_glaccount_array["GLAccountId"]);

                                                        tmpjObject["ItemFinancialInformation"]["ItemFinancialAccounts"]["Income"]["GLAccount"]["GLAccountId"] = GLAccountId;
                                                        tmpjObject["ItemFinancialInformation"]["ItemFinancialAccounts"]["Income"]["GLAccount"]["GLAccountCode"] = Income;
                                                        tmpjObject["ItemFinancialInformation"]["ItemFinancialAccounts"]["Income"]["GLAccount"]["Name"] = GLAccountName;
                                                    }
                                                    else
                                                    {
                                                        // remove ItemFinancialAccounts -> Income
                                                        var t = (JObject)tmpjObject.SelectToken("ItemFinancialInformation.ItemFinancialAccounts");
                                                        t.Property("Income").Remove();
                                                    }
                                                }
                                                else
                                                {
                                                    // remove ItemFinancialAccounts -> Income         
                                                    var t = (JObject)tmpjObject.SelectToken("ItemFinancialInformation.ItemFinancialAccounts");
                                                    t.Property("Income").Remove();
                                                }

                                                if (string.IsNullOrWhiteSpace(FinancialEntity) == false)
                                                {
                                                    var financialentity_token = financialentity_array.Where(x => x["EntityCode"]?.ToString() == FinancialEntity
                                                    && x["IsDefault"]?.ToString() == "True").FirstOrDefault();

                                                    if (financialentity_token != null)
                                                    {
                                                        string FinancialEntityId = Convert.ToString(financialentity_token["FinancialEntityId"]);
                                                        tmpjObject["ItemFinancialInformation"]["FinancialEntity"]["FinancialEntityId"] = FinancialEntityId;
                                                    }
                                                }

                                                tmpjObject["Name"] = Name;
                                                tmpjObject["ItemCode"] = ProductCode;
                                                tmpjObject["Description"] = Description;
                                                tmpjObject["PublishingInformation"]["StartDate"] = EffectiveDate;
                                                tmpjObject["PublishingInformation"]["ExpirationDate"] = EndingDate;
                                                tmpjObject["ItemId"] = ItemIdobj.ToString();

                                                if (decimal.TryParse(PriceStandard, out decimal t_Price))
                                                    tmpjObject["TempDefaultPrice"] = t_Price;

                                                #endregion

                                                #region Api call
                                                string ItemData = Newtonsoft.Json.JsonConvert.SerializeObject(tmpjObject);

                                                if (!string.IsNullOrWhiteSpace(ItemData))
                                                {
                                                    // Call Api
                                                    var client = new RestClient("https://bgcascripting.imiscloud.com/api/productitem");
                                                    var request = new RestRequest(Method.POST);

                                                    request.AddHeader("cache-control", "no-cache");
                                                    request.AddHeader("Content-Type", "application/json");
                                                    request.AddHeader("Authorization", "Bearer " + AccessToken);

                                                    request.AddParameter("undefined", ItemData, ParameterType.RequestBody);
                                                    IRestResponse response = client.Execute(request);

                                                    if (response.StatusCode == System.Net.HttpStatusCode.Created || response.StatusCode == System.Net.HttpStatusCode.OK)
                                                    {
                                                        //lblStatus.Text = "true";
                                                        txtResponse.Text += "Row no : " + i + " Created" + "\r\n";
                                                    }
                                                    else
                                                    {
                                                        //lblStatus.Text = "false";
                                                        //lblMessage.Text = $"{response.StatusDescription} {response.ErrorMessage}";
                                                        //lblErrorCode.Text = Convert.ToString(response.StatusCode);
                                                        txtExcelRowError.Text += "Row no : " + i + " Error : " + response.Content + "\r\n";
                                                    }
                                                }
                                                else
                                                    txtExcelRowError.Text += "Row no : " + i + " Error : No data found to post" + "\r\n";
                                                #endregion

                                                //jArray.Add(tmpjObject);
                                            }
                                            catch (Exception ex)
                                            {
                                                txtExcelRowError.Text += "Row no : " + i + " Error : " + ex.Message + "\r\n";
                                            }
                                        }
                                    }
                                    else
                                        MessageBox.Show("Unable to read SampleJson files");
                                }
                            }
                        }
                        else
                        {
                            if (financialentity_response.StatusCode != System.Net.HttpStatusCode.OK)
                                txtExcelRowError.Text = "Call Financial Entity api error : " + financialentity_response.Content + "\r\n";

                            if (glaccount_response.StatusCode != System.Net.HttpStatusCode.OK)
                                txtExcelRowError.Text += "Call GLAccount api error : " + glaccount_response.Content + "\r\n";

                            if (taxcategory_response.StatusCode != System.Net.HttpStatusCode.OK)
                                txtExcelRowError.Text += "Call TaxCategory api error : " + taxcategory_response.Content + "\r\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //lblStatus.Text = false.ToString();
                txtExcelRowError.Text += "Message : " + ex.Message + "\r\n";

                if (ex.InnerException != null)
                    txtExcelRowError.Text += " Inner Exception : " + ex.InnerException.Message;
            }

            // At Last Delete File
            if (!string.IsNullOrEmpty(FullFilePath))
            {
                if (File.Exists(FullFilePath))
                    File.Delete(FullFilePath);
            }
        }

        public class RestResponse
        {
            public bool Status { get; set; }
            public string Message { get; set; }
            public string Response { get; set; }
            public string ErrorCode { get; set; }
        }

        public class TokenResponse
        {
            public string access_token { get; set; }
            public string token_type { get; set; }
            public string expires_in { get; set; }
            public string userName { get; set; }
            public string issued { get; set; }
            public string expires { get; set; }
            public string error { get; set; }
        }
    }
}